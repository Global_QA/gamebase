import React, {Component} from "react";
import './App.css';
import Footer from './Components/Navigation/Footer/FooterBar/Footer';
import Header from './Components/Navigation/Header/HeaderBar/Header';
import MainBox from "./Components/Navigation/MainBox/MainBox";
import UpperBanner from './Components/Banners/UpperBanner/UpperBanner';
import BottomBanner from './Components/Banners/BottomBanner/BottomBanner';
import { ThemeContext } from "./Helpers/context.helper";

class App extends Component {

    state = {
        theme: "bannerLightTheme",
    };

    changeTheme = ( theme ) => _ => {
        this.setState ({ theme });
    };

    render = () => {

        const { theme } = this.state;
        const { changeTheme } = this;

        return (

            <ThemeContext.Provider value = {{
                theme,
                changeTheme
                }}>

                <div className="App">
                  <Header />
                      <UpperBanner />
                          <MainBox/>
                      <BottomBanner />
                  <Footer />
                </div>
            </ThemeContext.Provider>
          );//return
        };//render
    };//App

export default App;
