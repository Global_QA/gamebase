import React, {Component} from "react";
import LeftBanner from './../../../Components/Banners/LeftBanner/LeftBanner';
import RightBanner from './../../../Components/Banners/RightBanner/Right Banner';
import GameBox from "./../../../Components/GameBox/GameBox";
import styles from "./MainBox.module.css";



const MainBox = () => {
    return (
        <div id="mainBox" className={[styles.mainBox, styles.lightTheme].join(' ')}>
            <LeftBanner />
                <GameBox />
            <RightBanner />
        </div>
        )
    }
export default MainBox;