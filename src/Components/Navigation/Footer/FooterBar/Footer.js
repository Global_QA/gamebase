import React from "react";
import styles from './Footer.module.css';

function Footer() {
    return (
        <div id="footer" className={styles.lightTheme}>
            <h1 >GameBase: Enjoy playing</h1>
            <p>Copyright of GameBase 2021. All rights reserved</p>
        </div>
    );
}

export default Footer;