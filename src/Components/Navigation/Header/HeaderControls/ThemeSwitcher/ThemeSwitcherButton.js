import React from 'react';
import 'antd/dist/antd.css';
import { Switch } from 'antd';
import { CloseOutlined, CheckOutlined } from '@ant-design/icons';
import { ThemeContext } from "./../../../../../Helpers/context.helper";

const ThemeSwitcherButton = ({ children, action }) => {

    return(
        <ThemeContext.Consumer>
            {
                ( value ) => {
                    return (
                        <Switch onChange={ action }
                            checkedChildren="Dark"
                            unCheckedChildren="Light"
                            defaultChecked size="large"
                        >
                            {children}
                        </Switch>
                    );//return
                }
            }
        </ThemeContext.Consumer>
    );//return

};//ThemeSwitcherButton

export default ThemeSwitcherButton