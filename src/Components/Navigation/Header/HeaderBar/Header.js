import React, {Component} from "react";
import styles from './Header.module.css';
//=========== antd ==========
import 'antd/dist/antd.css';
import { Menu, Dropdown, Button, message, Space, Tooltip, Switch } from 'antd';
import { CloseOutlined, CheckOutlined , DownOutlined, UserOutlined } from '@ant-design/icons';
//=========== react-router-dom ==========
import Miner from  "./../../../Games/Miner/index";
import Maze from  "./../../../Games/Maze/index";
import TicTacToe from  "./../../../Games/TicTacToe/index";
import {Route, BrowserRouter, Link} from "react-router-dom";
//=========== Helpers ==============
import ThemeSwitcherButton from "./../HeaderControls/ThemeSwitcher/ThemeSwitcherButton";

const handleGamesMenuClick = (e) => {
  message.info(`You clicked menu item: ${e.key}`);
  console.log('click', e);
}

const handleLangMenuClick = (e) => {
  message.info(`You selected ${e.key} language`);
  console.log('click', e);
}




class Header extends Component {

    render = () => {
        return (
            <div className={[styles.header, styles.lightTheme].join(' ')}>
               <span>Hi, welcome on board{' '}</span>
           </div>
        )//return
    }//render
}//class Header

export default Header