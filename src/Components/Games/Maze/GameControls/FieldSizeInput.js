//форма звідси https://kanby.medium.com/%D0%B2%D0%B0%D0%BB%D0%B8%D0%B4%D0%B0%D1%86%D0%B8%D1%8F-%D1%84%D0%BE%D1%80%D0%BC-%D0%B2-reactjs-56341da641d0
//інпут звідси https://snipp.ru/html-css/input-type-number
import React, {Component} from 'react';
import styles from './FieldSizeInput.module.css';

class FieldSizeInput extends Component {
    render() {
        return (
            <form className="demoForm">

                <label htmlFor="width">Let's mine: Width: </label>
                <input type="number"
                       min="2" max="20"
                       className="form-control"
                       name="width"/>

                <label htmlFor="height">Height</label>
                <input type="number"
                       min="2" max="20"
                       className="form-control"
                       name="height"/>

                <span>  </span>

                <button type="submit" className="btn btn-primary">
                    <h3>Let Insanity start!</h3>
                </button>


            </form>
        )
    }
}

export default FieldSizeInput;