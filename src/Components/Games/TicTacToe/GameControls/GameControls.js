import React from "react";
import styles from "./GameControls.module.css"
import FieldSizeInput from "./FieldSizeInput";

function GameControls() {
    return (
        <div className={styles.lightTheme}>
            <FieldSizeInput/>
        </div>
    );
}

export default GameControls;