import React from "react";
import styles from "./GameScore.module.css"

const GameScore = () => {
    return(
        <div className={[styles.LightTheme, styles.lightTheme].join(' ')}>
            <h1>Game Score Here</h1>
        </div>
    )
};

export default GameScore;