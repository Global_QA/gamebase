import React, {Component} from "react";
import styles from "./GameField.module.css";
import img1 from "./../IMG/1.png";
import img2 from "./../IMG/2.png";
import img3 from "./../IMG/3.png";
import img4 from "./../IMG/4.png";
import img5 from "./../IMG/5.png";
import img6 from "./../IMG/6.png";
import img7 from "./../IMG/7.png";
import img8 from "./../IMG/8.png";
import imgBomba from "./../IMG/bomba.png";
import imgFail from "./../IMG/fail.png";
import imgIncognita from "./../IMG/incognita.png";
import imgOK from "./../IMG/ok.png";



class GameField extends React.Component {

    state = {
        GameStarted: true,
        GameOver: false
    };

    render() {
        if(this.state.GameStarted){
            return <div id="gameField" className={[styles.gameField, styles.lightTheme].join(' ')}>
                <h1>Miner: cip, cip, cip</h1>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img1}/>
                <img src={img2}/>
                <img src={img3}/>
                <img src={img4}/>
                <img src={img5}/>
                <img src={img6}/>
                <img src={img7}/>
                <img src={img8}/>
                <img src={imgBomba}/>
                <img src={imgIncognita}/>
                <img src={imgFail}/>
                <img src={imgOK}/>
            </div>
        } else {
            return <div className={[styles.gameField, styles.lightTheme].join(' ')}>
                {/*need to pass it to GameControls*/}
                <h1>{document.documentElement.scrollWidth} * {document.documentElement.scrollHeight} </h1>
                <h1>{document.documentElement.scrollWidth} * {document.documentElement.scrollHeight} </h1>
            </div>
        }

    };
}

export default GameField