import React from "react";
import styles from "./GameScore.module.css";
import gameScore from "./../../../GameBox/GameBox";
import youWon from "./../../../GameBox/GameBox";

const GameScore = (props) => {

    const youWonMessage = (props) => {


    }

    return(
        <div className={[styles.LightTheme, styles.lightTheme].join(' ')}>
            <h1>Your current score is: {props.gameScore}</h1>
            {props.youWon ? <h1><b>Congratulations!!! You WON !!!</b></h1> : <span/>}
            <p>Scroll down to restart your game</p>
        </div>
    )
};

export default GameScore;