import React from "react";
import styles from "./GameControls.module.css"
import { Radio, Button } from 'antd';
import 'antd/dist/antd.css';


const GameControls = (props) => {

    const [value, setValue] = React.useState(0)

    const onChange = e => {
            setValue(e.target.value);
        };

    const buttonHandler = (gameDifficulty) => {
        props.updateGameDifficulty(gameDifficulty);

        };

    return (
        <div className={styles.lightTheme}>

            <span>
                <Radio.Group onChange={onChange} value={value} size="large">
                <Radio value={0}><h2>Mad</h2></Radio>
                <Radio value={2}><h2>Dangerous</h2></Radio>
                <Radio value={4}><h2>Insane</h2></Radio>
                </Radio.Group>
            </span>

            <span>
                <Button
                    type="primary"
                    shape="round"
                    size="large"
                    onClick={() => buttonHandler(value)} >
                    <b>Let Insanity Starts!!!</b>
                </Button>
            </span>

        </div>
    ); // return
}; // GameControls

export default GameControls;