import React, {Component} from "react";
import styles from "./GameField.module.css";
import GameCell from './GameCell/GameCell';


class GameField extends React.Component {

    state = {

        maxHcells: 0,
        maxVcells: 0,
        bombasQnTT: 0,
        bombasOrder: [],
        bombasArea: [],
        clickedCells: 0
    };

    componentDidMount() {

        const maxHcells = ((this.divElement.clientWidth-10)/50 >> 0);
        const maxVcells = (maxHcells*window.screen.availHeight/window.screen.availWidth >> 0);
        this.setState({ maxHcells });
        this.setState({ maxVcells });

        const bombasQnTT = Math.floor((this.props.gameDifficulty+1)*0.1*maxHcells*maxVcells + Math.random() * ((this.props.gameDifficulty+1)*0.2*maxHcells*maxVcells + 1 - 0.2*maxHcells*maxVcells));

        const bombasOrder = [];
        const bombasArea = [];

        const bombSeed = (bombasQnTT) =>{

// creating new array with required bombas quantity
            for(let i = 0 ; i < maxHcells*maxVcells; i++){
                bombasQnTT > 0 ? bombasOrder.push(1) : bombasOrder.push(0);
                bombasQnTT--;
            };

// shuffling taken from here https://qna.habr.com/q/163095
            for (let i = bombasOrder.length-1 ; i > 0 ; i--){
                let j = Math.floor(Math.random() * (i + 1));
                let temp = bombasOrder[i];
                bombasOrder[i] = bombasOrder[j];
                bombasOrder[j] = temp;
            };
            this.setState({bombasOrder})

        };

        this.setState({bombasQnTT});

        bombSeed(bombasQnTT);

// new matrix with bombas from https://ru.stackoverflow.com/questions/868928
        const bombSpread = (bombasOrder, maxHcells, bombasArea) =>{
        for (let i = 0; i < bombasOrder.length; i++) {
           !(i % maxHcells) && bombasArea.push([]);
           bombasArea[i / maxHcells << 0].push(bombasOrder[i]);
        }
        this.setState({bombasArea});

              };

        bombSpread(bombasOrder, maxHcells, bombasArea);

    };//componentDidMount

    updateClickedCells = (value) => {
          this.setState({ clickedCells: this.state.clickedCells+value })

          this.props.updateYouWon(
              ((this.state.maxHcells*this.state.maxVcells
              - this.state.bombasQnTT - this.state.clickedCells === 1) && !this.props.gameOver  ? 1 : 0)
          )
        }

    render() {


            if(this.props.gameStarted && !this.props.gameOver){
            const { maxHcells, maxVcells, bombasOrder, bombasArea} = this.state;
            const hCells = Array.from({ length: maxHcells });
            const vCells = Array.from({ length: maxVcells });


            return(
            <div id="gameField"
                className={[styles.gameField, styles.lightTheme].join(' ')}
                ref={ (divElement) => { this.divElement = divElement } }>


                {vCells.map( (item, index) => {
                    // Rows
                    return hCells.map( (cell, key) => {
                        // Cells
                        return (
                                <GameCell

                                charged = {bombasOrder[index*maxHcells+key]}

                                nextDoors = {(
                                (index === 0 || key === 0 ? 0 : bombasArea[index-1][key-1]) +
                                (index === 0 ? 0 : bombasArea[index-1][key]) +
                                (index === 0 || key === (maxHcells-1) ? 0 : bombasArea[index-1][key+1]) +
                                (key === 0 ? 0 : bombasArea[index][key-1]) +
                                (key === (maxHcells-1) ? 0 : bombasArea[index][key+1]) +
                                (index === (maxVcells-1) || key === 0 ? 0 : bombasArea[index+1][key-1]) +
                                (index === (maxVcells-1) ? 0 : bombasArea[index+1][key]) +
                                (index === (maxVcells-1) || key === (maxHcells-1) ? 0 : bombasArea[index+1][key+1])
                                )}

                                gameOver = {this.props.gameOver}
                                updateGameOver={this.props.updateGameOver}
                                updateGameScore={this.props.updateGameScore}
                                updateClickedCells = {this.updateClickedCells}
                                />
                            );
                        })
                    })
                }

            </div>)

        } else {
            const hCells = Array.from({ length: this.state.maxHcells });
            const vCells = Array.from({ length: this.state.maxVcells });
            return(
                <div id="gameField" className={[styles.gameField, styles.lightTheme].join(' ')}>
                     {vCells.map( (item, index) => {
                                        // Rows
                                        return hCells.map( (cell, key) => {
                                            // Cells
                                            return (

                                                <GameCell

                                                    charged = {this.state.bombasOrder[index*this.state.maxHcells+key]}
                                                    gameOver = {this.props.gameOver}
                                                />

                                            )
                                        });
                                    })
                                }
                </div>
            )
        }

    };//render
}

export default GameField