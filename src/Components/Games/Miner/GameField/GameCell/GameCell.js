import React, {Component} from 'react';
import styles from "./GameCell.module.css";
import img1 from "./IMG/1.png";
import img2 from "./IMG/2.png";
import img3 from "./IMG/3.png";
import img4 from "./IMG/4.png";
import img5 from "./IMG/5.png";
import img6 from "./IMG/6.png";
import img7 from "./IMG/7.png";
import img8 from "./IMG/8.png";
import imgBomba from "./IMG/bomba.png";
import imgFail from "./IMG/fail.png";
import imgIncognita from "./IMG/incognita.png";
import imgOK from "./IMG/ok.png";

class GameCell extends React.Component {

    state = {
        cellImg: imgIncognita,
        cellWasClicked: false
        }


    cellHandler = (charged, nextDoors) => {

    if(this.state.cellWasClicked) {
        console.log("User tried to click cell twice")
    } else {
        this.props.updateClickedCells(1)

            if(charged){
                this.setState({cellImg: imgFail});
                this.setState({cellWasClicked: true});
                this.props.updateGameOver(true);
            }else{
                this.setState({cellWasClicked: true})
                this.props.updateGameScore(1);

                switch(nextDoors){
                    case 0:
                        this.setState({cellImg: imgOK});
                        break;

                    case 1:
                        this.setState({cellImg: img1});
                        break;

                    case 2:
                        this.setState({cellImg: img2});
                        break;

                    case 3:
                        this.setState({cellImg: img3});
                        break;

                    case 4:
                        this.setState({cellImg: img4});
                        break;

                    case 5:
                        this.setState({cellImg: img5});
                        break;

                    case 6:
                        this.setState({cellImg: img6});
                        break;

                    case 7:
                        this.setState({cellImg: img7});
                        break;

                    case 8:
                        this.setState({cellImg: img8});
                        break;
                }
           }
        }
    };//cellHandler

    render(){

        const { charged, nextDoors, gameOver } = this.props;
        const { cellHandler } = this;
        const { cellImg, cellWasClicked } = this.state;

if (!gameOver){

        return(
            <img src={cellImg}
             className={styles.cellSize}
             onClick={() => cellHandler(charged, nextDoors)}
            />
        );//return

    }else{
 if
            (!cellWasClicked && charged){
                return(
                    <img src={imgBomba}
                     className={styles.cellSize}
                     />
                )//return
        } else if
            (cellWasClicked && charged){
                return(
                    <img src={imgFail}
                     className={styles.cellSize}
                     />
                )//return
        } else {
                return(
                    <img src={cellImg}
                     className={styles.cellSize}
                     />
            )//return
        }

    }
};//render

};//GameCell

export default GameCell;