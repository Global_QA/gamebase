import React from "react";
import styles from './BottomBanner.module.css';
import COVIDtitle from "./COVIDtitle.png"
const BottomBanner = () => {
    return (
        <div id="bottomBanner" className={[styles.bottomBanner, styles.bannerLightTheme].join(' ')}>
           <a href = "https://www.diy.ua">
               <img src ={COVIDtitle}
               title = {"DIY.ua - актуальна інформація по COVID в Україні і світі"}/>
           </a>
        </div>
    );
}

export default BottomBanner