import React from "react";
import styles from './LeftBanner.module.css';
import COVIDstatistic from "./COVIDstatistics.png"
const LeftBanner = () => {
    return (
        <div id="leftBanner" className={[styles.leftBanner, styles.bannerLightTheme].join(' ')}>
           <a href = "https://www.diy.ua">
               <img src ={COVIDstatistic}
               width = "160" height = "600"
               title = {"DIY.ua - актуальна інформація по COVID в Україні і світі"}/>
           </a>
        </div>
    );
}

export default LeftBanner