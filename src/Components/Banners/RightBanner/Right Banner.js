import React from "react";
import styles from './RightBanner.module.css';
import DIYvertical from './DIYvertical.png';
import UAcvUAvertical from './UAcvUAvertical.png';

const RightBanner = () => {
    return (
        <div id="rightBanner" className={[styles.rightBanner, styles.bannerLightTheme].join(' ')}>
            <a href = "https://ua.cv.ua">
               <img src ={UAcvUAvertical}
               title = {"ua.cv.ua - відеоз'йомка і монтаж"}/>
            </a>
        </div>
    );
}

export default RightBanner