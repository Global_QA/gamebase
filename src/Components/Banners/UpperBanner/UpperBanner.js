import React from "react";
import styles from './UpperBanner.module.css';
import DIYvisitka from "./DIYvisitka.png";
import UAcvUAvizitka from "./UAcvUAvizitka.png"

const UpperBanner = () => {
    return (
        <div id="upperBanner" className={[styles.upperBanner, styles.bannerLightTheme].join(' ')}>
            <a href = "https://ua.cv.ua">
                <img src ={UAcvUAvizitka}
                width = {"728"}
                height = {"90"}
                title = {"ua.cv.ua - відеоз'йомка і монтаж"}/>
            </a>
        </div>
    );
}

export default UpperBanner