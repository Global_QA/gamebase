import React, {Component} from "react";
import styles from "./GameBox.module.css"
import GameScore from './../Games/Miner/GameScore/GameScore';
import GameField from "./../Games/Miner/GameField/GameField";
import GameControls from "./../Games/Miner/GameControls/GameControls";


class GameBox extends React.Component {

    state = {
        gameStarted: true,
        gameOver: false,
        gameDifficulty: 0,
        keyValue: 1,
        gameScore: 0,
        youWon: 0
    }; // state

    updateGameOver = (value) => {
            this.setState({ gameOver: value });
        };

    updateGameDifficulty = (value) => {
            this.setState({ gameDifficulty: value })
            this.setState({ keyValue: ++this.state.keyValue})
            this.setState({ gameOver: false})
            this.setState({ gameScore: 0})
            this.setState({ youWon: 0})
        }

    updateGameScore = (value) => {
            this.setState({ gameScore: this.state.gameScore+value*(this.state.gameDifficulty+1) })
    }

    updateYouWon = (value) => {
            this.setState({ youWon: value })


    }

    render () {

        const { gameDifficulty, gameStarted, gameOver, keyValue, gameScore, youWon} = this.state;
        const { updateGameOver, updateGameDifficulty, updateGameScore, updateYouWon } = this;

    return (
                <div id="GameBox" className={[styles.gameBox, styles.lightTheme].join(' ')}>
                    <GameScore
                        gameScore = {gameScore}
                        youWon = {youWon}
                    />

                    <GameField
                        gameDifficulty = {gameDifficulty}
                        gameStarted = {gameStarted}
                        gameOver = {gameOver}
                        updateGameOver = {updateGameOver}
                        key = {keyValue}
                        updateGameScore = {updateGameScore}
                        updateYouWon = {updateYouWon}
                    />

                    <GameControls
                        updateGameDifficulty = {updateGameDifficulty}
                    />
                </div>
            ) //return
    }; // render

}; // GameBox

export default GameBox;