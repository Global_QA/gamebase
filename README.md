# WTElf #

**This is a React.JS-based SPA with:**

 - Multilingual Interface
 - Dark / Light themes
 - Users' Registration facility
 - Playing different games as guest or registered user
 - Reviewing own achievements

### App's Key Elements ###

 - **Header:**
   - Language selector
   - Theme selector
   - Register / Login buttons
     - Register modal popup
     - Login modal popup
     - Forgot pass modal popup
 
- **Footer**
   - Copyright info
   - Contact-us info
   - Subscribe info 
 
 - **GameBox**
   - Left add banner
   - Game controls area
   - GameField
 
 - **User's personal area**
   - Profile
     - Edit profile modal popup
     - Change password modal popup
   - Achievements history

### Setup & Run ###
For components pls see https://github.com/ant-design/ant-design ";


